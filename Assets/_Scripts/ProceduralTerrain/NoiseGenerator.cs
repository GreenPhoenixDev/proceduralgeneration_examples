﻿using UnityEngine;

public class NoiseGenerator
{
	private int seed;
	public int Seed
	{
		get { return seed; }
		set
		{
			seed = value;
			Random.InitState(seed);
			xOffset = Random.value * 1000f;
			yOffset = Random.value * 1000f;
		}
	}
	private float xOffset;
	private float yOffset;
	private float perlinScale;
	private int octaves;
	private float lacunarity;
	private float gain;

	public NoiseGenerator(int _seed, float _perlinScale, int _octaves = 3, float _lacunarity = 1.2f, float _gain = 0.5f)
	{
		//seed = _seed;
		seed = 1;
		Random.InitState(seed);
		xOffset = Random.value * 1000f;
		yOffset = Random.value * 1000f;
		perlinScale = _perlinScale;
		octaves = _octaves;
		lacunarity = _lacunarity;
		gain = _gain;
	}

	public float GetValueNoise()
	{
		return Random.value;
	}
	public float GetPerlinNoise(float x, float y, int xResOffset, int yResOffset)
	{
		return Mathf.PerlinNoise((x + xOffset + xResOffset) * perlinScale, (y + yOffset + yResOffset) * perlinScale);
		//return Mathf.PerlinNoise((x + xResOffset) * perlinScale, (y + yResOffset) * perlinScale);
	}
	public float GetFractalNoise(float x, float y, int xResOffset, int yResOffset)
	{
		float fractalNoise = 0f;
		float frequency = 1f;
		float amplitude = 1f;
		for (int j = 0; j < octaves; j++)
		{
			float xVal = x * frequency;
			float yVal = y * frequency;
			fractalNoise += amplitude * GetPerlinNoise(xVal, yVal, xResOffset, yResOffset);
			frequency *= lacunarity;
			amplitude *= gain;
		}
		return fractalNoise;
	}
}
