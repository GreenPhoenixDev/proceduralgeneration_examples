﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainGenerator : MonoBehaviour
{
	[Header("Terrain Settings")]
	[SerializeField] private KeyCode kc_GenerateTerrain = KeyCode.Space;
	[SerializeField] private KeyCode kc_UpdateSeed = KeyCode.O;
	[SerializeField] private float perlinScale = 1f;
	[SerializeField, Range(0f, 3f)] private float heigthScale = 0.4f;
	[SerializeField] int seed = 42;
	[SerializeField] private int octaves = 3;
	[SerializeField] private float lacunarity = 2f;
	[SerializeField] private float gain = 0.8f;
	[SerializeField] private Terrain[] terrains;
	[Header("")]
	[SerializeField] Texture2D heightTex;
	private NoiseGenerator noise;
	private float realPerlinScale;

	void Update()
	{
		if (Input.GetKeyDown(kc_GenerateTerrain))
		{
			//if (noise == null)
			noise = new NoiseGenerator(seed, realPerlinScale, octaves, lacunarity, gain);
			//else
			//	UpdateSeed();

			for (int i = 0; i < terrains.Length; i++)
			{
				int res = terrains[i].terrainData.heightmapResolution;
				float[,] heigths = new float[res, res];

				float xResOffsetFactor = terrains[i].transform.position.z / terrains[i].terrainData.size.z;
				float yResOffsetFactor = terrains[i].transform.position.x / terrains[i].terrainData.size.x;

				realPerlinScale = perlinScale * 0.01f;
				for (int y = 0; y < res; y++)
				{
					for (int x = 0; x < res; x++)
					{
						float heightMapFactor = 1f;
						if (heightTex != null)
						{
							int texX = Mathf.RoundToInt((float)x / res * heightTex.width);
							int texY = Mathf.RoundToInt((float)y / res * heightTex.height);
							heightMapFactor = heightTex.GetPixel(texX, texY).r;
							heigths[x, y] = noise.GetFractalNoise(x, y, (res - 1) * (int)xResOffsetFactor, (res - 1) * (int)yResOffsetFactor) * heigthScale * heightMapFactor;
						}
						else
						{
							heigths[x, y] = noise.GetFractalNoise(x, y, (res - 1) * (int)xResOffsetFactor, (res - 1) * (int)yResOffsetFactor) * heigthScale;
						}
					}
				}
				terrains[i].terrainData.SetHeights(0, 0, heigths);
			}
		}

		if (Input.GetKeyDown(kc_UpdateSeed))
		{
			for (int i = 0; i < terrains.Length; i++)
			{
				UpdateSeed(i);
			}
		}
	}

	void UpdateSeed(int i)
	{
		noise.Seed = seed;
	}
}