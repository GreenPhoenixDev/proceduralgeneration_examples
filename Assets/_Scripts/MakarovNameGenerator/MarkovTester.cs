﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarkovTester : MonoBehaviour
{
	public string[] Names;
	public List<string> NamesFromFile;
	public int order = 3;
	public int minLength = 8;

	private MarkovNameGenerator nameGen;

	void Start()
	{
		TextAsset nameAsset = Resources.Load("MaleNames") as TextAsset;
		NamesFromFile = new List<string>(nameAsset.text.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries));

		nameGen = new MarkovNameGenerator(NamesFromFile, order, minLength);
		for (int i = 0; i < 10; i++)
		{
			var name = nameGen.NextName;
			Debug.Log(name);
		}
	}
}
