﻿using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class SierpinskiTriangle : MonoBehaviour
{
	[SerializeField] private GameObject twigPrefab;
	[SerializeField] private Transform result;
	[SerializeField] private string axiom = "F-G-G";
	[SerializeField] private float angle = 120f;
	[SerializeField] private float length = 1f;
	[SerializeField] private int iterations = 4;

	private Dictionary<char, string> rules = new Dictionary<char, string>();
	private string sentence;

	void Start()
	{
		result.gameObject.SetActive(false);
		sentence = axiom;
		rules.Add('F', "F-G+F+G-F");
		rules.Add('G', "GG");
		for (int i = 0; i < iterations; i++)
		{
			GenerateSentence();
		}
		GenerateMesh();
	}

	void GenerateSentence()
	{
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < sentence.Length; i++)
		{
			char curChar = sentence[i];

			if (rules.ContainsKey(curChar))
				sb.Append(rules[curChar]);
			else
				sb.Append(curChar);
		}

		sentence = sb.ToString();
	}
	void GenerateMesh()
	{
		for (int i = 0; i < sentence.Length; i++)
		{
			char curChar = sentence[i];

			if(curChar == 'F' || curChar == 'G')
			{
				var twig = Instantiate(twigPrefab, transform.position, transform.rotation, result.transform);
				// Move forward
				transform.Translate(Vector3.up * length);
				Combine(result);
				Destroy(twig);
			}
			else if(curChar == '+')
			{
				transform.Rotate(Vector3.forward * angle);
			}
			else if(curChar == '-')
			{
				transform.Rotate(Vector3.back * angle);
			}
		}
	}

	void Combine(Transform result)
	{
		MeshFilter[] meshFilters = result.GetComponentsInChildren<MeshFilter>();
		CombineInstance[] combineInstances = new CombineInstance[meshFilters.Length];
		for (int i = 0; i < meshFilters.Length; i++)
		{
			combineInstances[i].mesh = meshFilters[i].sharedMesh;
			combineInstances[i].transform = meshFilters[i].transform.localToWorldMatrix;
			meshFilters[i].gameObject.SetActive(false);
		}
		result.GetComponent<MeshFilter>().mesh = new Mesh();
		result.GetComponent<MeshFilter>().mesh.CombineMeshes(combineInstances);
		result.gameObject.SetActive(true);
	}
}
