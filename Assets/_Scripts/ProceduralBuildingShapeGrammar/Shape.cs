﻿public abstract class Shape
{
	public virtual ShapeData Data { get; }

	protected ShapeType shapeType;
	public ShapeType ShapeType => shapeType;
	protected Shape nextShape;
	public Shape NextShape => nextShape;

	public Shape()
	{
	}
	public Shape(ShapeData _data)
	{
		Data = _data;
	}

	public abstract void Expand();
}

public enum ShapeType { Default, Ground, Floor, Roof }