﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class House : MonoBehaviour
{
	[SerializeField] private AnimationCurve planarOffsetCurve;
	[SerializeField] private List<Vector2> planarFloorOffsets = new List<Vector2>(32);
	[SerializeField] private List<float> angleOffsets = new List<float>(32);
	[SerializeField] private List<float> floorHeights = new List<float>(32);
	[SerializeField] private int maxNumFloors = 4;
	[SerializeField] private bool useVariation;
	[Header("House Settings")]
	[SerializeField, Range(1, 100)] private int numFloors = 1;
	[SerializeField, Range(1f, 100f)] private float frontLength = 1f;
	[SerializeField, Range(1f, 100f)] private float sideLength = 1f;
	[SerializeField, Range(0.1f, 5f)] private float groundFloorHeigth = 1f;
	[SerializeField, Range(0.1f, 5f)] private float roofHeigth = 1f;
	[SerializeField, Range(0.1f, 5f)] private float maxPlanarOffset;
	[SerializeField, Range(0.1f, 5f)] private float maxAngleOffset;
	[Header("Meshes")]
	[SerializeField] private Mesh[] floorModules;
	[SerializeField] private Mesh floorMesh;
	[SerializeField] private Mesh groundFloorMesh;
	[SerializeField] private Mesh roofMesh;
	[SerializeField] private Mesh bottomCapMesh;
	[SerializeField] private Mesh topCapMesh;

	public Material mat;

	private ShapeGrammar houseGrammar;
	private Transform groundFloor;
	private Transform roof;
	private Transform leftWall;
	private Transform rightWall;
	private Transform frontWall;
	private Transform backWall;
	private Transform topCap;
	private Transform bottomCap;
	private Transform floorParent;
	private LayerMask layer;
	private Vector3 pos;
	private Vector3 scale;
	private Quaternion rot;
	private Matrix4x4 matrix;
	private int numMeshes;
	private int numFloorsAtStart;
	private int[] randomIdxs = new int[32];
	private bool isHouseBuilt;

	#region MonoBehaviour
	void Start()
	{
		houseGrammar = new ShapeGrammar(
			maxNumFloors, 3f, 20f, maxPlanarOffset, maxAngleOffset,
			new bool[] { useVariation, useVariation, false });
		houseGrammar.Generate(BuildHouse);
		layer = LayerMask.NameToLayer("Default");
		// random indices for variation
		for (int i = 0; i < randomIdxs.Length; i++)
		{
			randomIdxs[i] = Random.Range(0, floorModules.Length);
		}
		numFloorsAtStart = numFloors;
	}
	void Update()
	{
		if (!isHouseBuilt) { return; }

		groundFloor.localScale = new Vector3(frontLength, groundFloorHeigth, sideLength);
		DrawGroundFloor(groundFloor);

		// foreach floor set position and render
		for (int f = 0; f < numFloors; f++)
		{
			DrawFloor(f);
		}
		roof.localPosition = Vector3.up * (numFloors + groundFloorHeigth);
		roof.localScale = new Vector3(frontLength, roofHeigth, sideLength);
		DrawRoof(roof);
	}
	#endregion

	#region Build the house
	void BuildHouse(HouseData data)
	{
		groundFloorHeigth = data.GroundFloor.Data.Heigth;
		roofHeigth = data.Roof.Data.Heigth;
		numFloors = data.Floors.Count;
		frontLength = data.GroundFloor.Data.Width;
		sideLength = data.GroundFloor.Data.Length;
		scale = Vector3.one;
		for (int i = 0; i < numFloors; i++)
		{
			planarFloorOffsets.Add(data.Floors[i].Data.PlanarOffset);
			angleOffsets.Add(data.Floors[i].Data.AngleOffset);
			floorHeights.Add(data.Floors[i].Data.Heigth);
		}

		// GameObjects to get Transforms
		GameObject floorParentGO = new GameObject();
		GameObject groundFloorGO = new GameObject();
		GameObject roofGO = new GameObject();
		GameObject leftWallGO = new GameObject();
		GameObject rightWallGO = new GameObject();
		GameObject frontWallGO = new GameObject();
		GameObject backWallGO = new GameObject();
		GameObject bottomCapGO = new GameObject();
		GameObject topCapGO = new GameObject();

		// hide helper GOs in hierarchy
		groundFloorGO.hideFlags = roofGO.hideFlags = leftWallGO.hideFlags
			= rightWallGO.hideFlags = frontWallGO.hideFlags = backWallGO.hideFlags
			= floorParentGO.hideFlags = bottomCapGO.hideFlags = topCapGO.hideFlags
			= HideFlags.HideInHierarchy;

		// get the transforms
		floorParent = floorParentGO.transform;
		groundFloor = groundFloorGO.transform;
		roof = roofGO.transform;
		leftWall = leftWallGO.transform;
		rightWall = rightWallGO.transform;
		frontWall = frontWallGO.transform;
		backWall = backWallGO.transform;
		bottomCap = bottomCapGO.transform;
		topCap = topCapGO.transform;

		// initialize with offsets
		Initialize(floorParent, Vector3.up * groundFloorHeigth);
		Initialize(groundFloor, Vector3.zero);
		Initialize(leftWall, new Vector3(frontLength * 0.5f, groundFloorHeigth, 0f));
		InitializeAngle(leftWall, 90f);
		Initialize(rightWall, new Vector3(-frontLength * 0.5f, groundFloorHeigth, 0f));
		InitializeAngle(rightWall, 270f);
		Initialize(frontWall, new Vector3(0f, groundFloorHeigth, sideLength * 0.5f));
		Initialize(backWall, new Vector3(0f, groundFloorHeigth, -sideLength * 0.5f));
		InitializeAngle(backWall, 180f);
		Initialize(bottomCap, Vector3.zero);
		Initialize(topCap, Vector3.zero);

		float roofYOffset = numFloors + groundFloorHeigth;
		Initialize(roof, new Vector3(0f, roofYOffset, 0f));
		isHouseBuilt = true;
	}
	void Initialize(Transform t, Vector3 posOffset)
	{
		t.parent = transform;
		t.localPosition = posOffset;
	}
	void InitializeAngle(Transform t, float angle)
	{
		t.localRotation = Quaternion.Euler(0f, angle, 0f);
	}
	#endregion

	#region Updated DrawMeshes
	void DrawGroundFloor(Transform groundFloor)
	{
		Vector3 offset = Vector3.up * groundFloorHeigth * 0.5f;
		Quaternion newRot = transform.rotation;
		Vector3 newPos = transform.position + (newRot * offset);
		matrix = Matrix4x4.TRS(newPos, newRot, groundFloor.localScale);
		Graphics.DrawMesh(groundFloorMesh, matrix, mat, layer);
	}
	void DrawFloor(int f)
	{
		var wrappedF = f % numFloorsAtStart;
		float currentHeight = f + groundFloorHeigth;
		float currentAngle = angleOffsets[wrappedF];
		floorParent.localPosition = new Vector3(0f, currentHeight, 0f);
		floorParent.localRotation = Quaternion.Euler(new Vector3(0f, currentAngle, 0f));
		float plOffFromCurve = planarOffsetCurve.Evaluate((float)f / numFloors);

		if (planarFloorOffsets[wrappedF] != Vector2.zero)
		{
			bottomCap.localPosition = new Vector3(
				planarFloorOffsets[wrappedF].x * plOffFromCurve, f + groundFloorHeigth, 
				planarFloorOffsets[wrappedF].y * plOffFromCurve);
			DrawCap(bottomCap, bottomCapMesh);

			topCap.localPosition = new Vector3(
				planarFloorOffsets[wrappedF].x * plOffFromCurve, f + groundFloorHeigth + 1f,
				planarFloorOffsets[wrappedF].y * plOffFromCurve);
			DrawCap(topCap, topCapMesh);
		}

		Vector2 offsetV2 = planarFloorOffsets[wrappedF] * plOffFromCurve;
		Vector3 offset = new Vector3(offsetV2.x, 0f, offsetV2.y);
		leftWall.localPosition = new Vector3(frontLength * 0.5f, f + groundFloorHeigth, 0f) + offset;
		rightWall.localPosition = new Vector3(-frontLength * 0.5f, f + groundFloorHeigth, 0f) + offset;
		frontWall.localPosition = new Vector3(0f, f + groundFloorHeigth, sideLength * 0.5f) + offset;
		backWall.localPosition = new Vector3(0f, f + groundFloorHeigth, -sideLength * 0.5f) + offset;
		DrawWall(leftWall, sideLength, currentAngle);
		DrawWall(rightWall, sideLength, currentAngle);
		DrawWall(frontWall, frontLength, currentAngle);
		DrawWall(backWall, frontLength, currentAngle);
	}
	void DrawWall(Transform wall, float wallLength, float angle)
	{
		numMeshes = Mathf.FloorToInt(wallLength);
		float remaining = wallLength % 1;
		float offset = remaining / numMeshes;
		Vector3 newPos;
		Quaternion newRot = transform.rotation * wall.localRotation * Quaternion.Euler(Vector3.up * angle);
		Vector3 newScale = new Vector3(scale.x + offset, scale.y, scale.z);
		matrix = Matrix4x4.TRS(wall.position, Quaternion.identity, Vector3.one);
		for (int i = 0; i < numMeshes; i++)
		{
			float j = i - (numMeshes - 1) * 0.5f;
			newPos = wall.position + wall.right * j + wall.right * offset * j;
			matrix.SetTRS(newPos, newRot, newScale);
			int idx = randomIdxs[i % randomIdxs.Length];
			Graphics.DrawMesh(floorModules[idx], matrix, mat, layer);
		}
	}
	void DrawCap(Transform cap, Mesh capMesh)
	{
		Quaternion newRot = floorParent.rotation;
		Vector3 newPos = cap.position;
		Vector3 scale = new Vector3(frontLength, 1f, sideLength);
		matrix = Matrix4x4.TRS(newPos, newRot, scale);
		Graphics.DrawMesh(capMesh, matrix, mat, layer);
	}
	void DrawRoof(Transform roof)
	{
		Quaternion newRot = transform.rotation;
		Vector2 planarOffsetV2 = planarFloorOffsets[(numFloors - 1) % numFloorsAtStart];
		Vector3 planarOffset = new Vector3(planarOffsetV2.x, 0f, planarOffsetV2.y) * 
			planarOffsetCurve.Evaluate((numFloors - 1) / (float)numFloors);
		Vector3 offset = planarOffset + Vector3.up * roof.localScale.y * 0.5f;
		Vector3 newPos = roof.position + (newRot * offset);
		matrix = Matrix4x4.TRS(newPos, newRot, roof.localScale);
		Graphics.DrawMesh(roofMesh, matrix, mat, layer);
	}
	#endregion
}