﻿using System.Collections.Generic;

public class HouseData
{
	public Roof Roof { get; set; }
	public GroundFloor GroundFloor { get; set; }
	public List<Floor> Floors { get; set; }

	public HouseData(List<Floor> floors)
	{
		Floors = floors;
	}
}