﻿using UnityEngine;

public class Floor : Shape
{
	private ShapeData myData;
	public override ShapeData Data => myData;
	public int FloorIdx { get; }

	public Floor() : base()
	{
		shapeType = ShapeType.Floor;
	}
	public Floor(ShapeData data, int floorIdx) : base(data)
	{
		Vector2 planarOffset = data.PlanarOffset;
		float angleOffset = data.AngleOffset;
		FloorIdx = floorIdx;

		if (data.Randomizers[0])
		{
			planarOffset = new Vector2(
				Random.Range(-data.MaxPlanarOffset, data.MaxPlanarOffset),
				Random.Range(-data.MaxPlanarOffset, data.MaxPlanarOffset));
		}
		if (data.Randomizers[1])
		{
			angleOffset = Random.Range(0f, data.MaxAngleOffset);
		}
		myData = new ShapeData(data);
		myData.PlanarOffset = planarOffset;
		myData.AngleOffset = angleOffset;
		shapeType = ShapeType.Floor;
	}

	public override void Expand()
	{
		float decide = Random.value;
		if (decide > 0.1 && FloorIdx < myData.MaxFloors)
		{
			nextShape = new Floor(Data, FloorIdx + 1);
			nextShape.Expand();
		}
		else
		{
			nextShape = new Roof(Data);
		}
	}
}