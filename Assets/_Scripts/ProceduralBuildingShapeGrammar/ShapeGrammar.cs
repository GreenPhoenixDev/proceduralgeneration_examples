﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class ShapeGrammar
{
	private Shape axiom;
	private Shape sentence;
	private int numFloors;
	private int maxFloors = 1;
	private float minXZSize = 1f;
	private float maxXZSize = 1f;
	private float maxPlanarOffset;
	private float maxAngleOffset;
	bool[] randomizers = new bool[3];

	public ShapeGrammar()
	{
	}
	public ShapeGrammar(float _minXZSize, float _maxXZSize, int _maxFloors)
	{
		minXZSize = _minXZSize;
		maxXZSize = _maxXZSize;
		maxFloors = _maxFloors;
	}
	public ShapeGrammar(int _maxFloors, float _minXZSize, float _maxXZSize, float _maxPlanarOffset, float _maxAngleOffset, bool[] _randomizers)
	{
		maxFloors = _maxFloors;
		minXZSize = _minXZSize;
		maxXZSize = _maxXZSize;
		maxPlanarOffset = _maxPlanarOffset;
		maxAngleOffset = _maxAngleOffset;
		randomizers = _randomizers;
	}

	public void Generate(Action<HouseData> buildHouse)
	{
		float width = Random.Range(minXZSize, maxXZSize);
		float length = Random.Range(minXZSize, maxXZSize);
		float heigth = 1f;
		axiom = new GroundFloor(new ShapeData(
			width, length, heigth, 0f, maxAngleOffset, Vector2.zero, maxPlanarOffset, maxFloors, randomizers)
			);
		sentence = axiom;
		sentence.Expand();
		HouseData houseData = new HouseData(new List<Floor>());

		while (sentence != null)
		{
			switch (sentence.ShapeType)
			{
				case ShapeType.Default:
					break;
				case ShapeType.Ground:
					houseData.GroundFloor = (GroundFloor)sentence;
					break;
				case ShapeType.Floor:
					houseData.Floors.Add((Floor)sentence);
					break;
				case ShapeType.Roof:
					houseData.Roof = (Roof)sentence;
					break;
				default:
					break;
			}
			sentence = sentence.NextShape;
		}
		buildHouse(houseData);
	}
}