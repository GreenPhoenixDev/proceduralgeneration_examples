﻿using UnityEngine;

public class ShapeData
{
	public float Width { get; set; }
	public float Length { get; set; }
	public float Heigth { get; set; }
	public float AngleOffset { get; set; }
	public float MaxAngleOffset { get; set; }
	public Vector2 PlanarOffset { get; set; }
	public float MaxPlanarOffset { get; set; }
	public int MaxFloors { get; set; }
	public bool[] Randomizers { get; set; }

	public ShapeData(ShapeData data)
	{
		Width = data.Width;
		Length = data.Length;
		Heigth = data.Heigth;
		AngleOffset = data.AngleOffset;
		MaxAngleOffset = data.MaxAngleOffset;
		PlanarOffset = data.PlanarOffset;
		MaxPlanarOffset = data.MaxPlanarOffset;
		MaxFloors = data.MaxFloors;
		Randomizers = data.Randomizers;
	}
	public ShapeData(float width, float length, float heigth, float angleOffset, float maxAngleOffset,
		Vector2 planarOffset, float maxPlanarOffset, int maxFloors, bool[] randomizers)
	{
		Width = width;
		Length = length;
		Heigth = heigth;
		AngleOffset = angleOffset;
		MaxAngleOffset = maxAngleOffset;
		PlanarOffset = planarOffset;
		MaxPlanarOffset = maxPlanarOffset;
		MaxFloors = maxFloors;
		Randomizers = randomizers;
	}
}